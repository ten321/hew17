report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew17_live_HEWeb_2017_Homepage_0_document_0_phone.png",
        "test": "../bitmaps_test/20220814-104901/backstop_hew17_live_HEWeb_2017_Homepage_0_document_0_phone.png",
        "selector": "document",
        "fileName": "backstop_hew17_live_HEWeb_2017_Homepage_0_document_0_phone.png",
        "label": "HEWeb 2017 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://2017.highedweb.org/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew17_live_HEWeb_2017_Homepage_0_document_1_tablet.png",
        "test": "../bitmaps_test/20220814-104901/backstop_hew17_live_HEWeb_2017_Homepage_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "backstop_hew17_live_HEWeb_2017_Homepage_0_document_1_tablet.png",
        "label": "HEWeb 2017 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://2017.highedweb.org/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew17_live_HEWeb_2017_Homepage_0_document_2_desktop.png",
        "test": "../bitmaps_test/20220814-104901/backstop_hew17_live_HEWeb_2017_Homepage_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "backstop_hew17_live_HEWeb_2017_Homepage_0_document_2_desktop.png",
        "label": "HEWeb 2017 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://2017.highedweb.org/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew17_live_HEWeb_2017_Schedule_0_document_0_phone.png",
        "test": "../bitmaps_test/20220814-104901/backstop_hew17_live_HEWeb_2017_Schedule_0_document_0_phone.png",
        "selector": "document",
        "fileName": "backstop_hew17_live_HEWeb_2017_Schedule_0_document_0_phone.png",
        "label": "HEWeb 2017 Schedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://2017.highedweb.org/schedule/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew17_live_HEWeb_2017_Schedule_0_document_1_tablet.png",
        "test": "../bitmaps_test/20220814-104901/backstop_hew17_live_HEWeb_2017_Schedule_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "backstop_hew17_live_HEWeb_2017_Schedule_0_document_1_tablet.png",
        "label": "HEWeb 2017 Schedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://2017.highedweb.org/schedule/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -2140
          },
          "misMatchPercentage": "11.84",
          "analysisTime": 106
        },
        "diffImage": "../bitmaps_test/20220814-104901/failed_diff_backstop_hew17_live_HEWeb_2017_Schedule_0_document_1_tablet.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew17_live_HEWeb_2017_Schedule_0_document_2_desktop.png",
        "test": "../bitmaps_test/20220814-104901/backstop_hew17_live_HEWeb_2017_Schedule_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "backstop_hew17_live_HEWeb_2017_Schedule_0_document_2_desktop.png",
        "label": "HEWeb 2017 Schedule",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://2017.highedweb.org/schedule/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": false,
          "dimensionDifference": {
            "width": 0,
            "height": -2154
          },
          "misMatchPercentage": "13.50",
          "analysisTime": 161
        },
        "diffImage": "../bitmaps_test/20220814-104901/failed_diff_backstop_hew17_live_HEWeb_2017_Schedule_0_document_2_desktop.png"
      },
      "status": "fail"
    }
  ],
  "id": "backstop_hew17_live"
});